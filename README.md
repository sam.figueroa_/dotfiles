# Da fuq?
This is a collection of my personal dot files and like wise that configure my working setup.

# Required/Favorite Packages & Gems
## Homebrew
Take a look at the included `Brewfile` or just run `brew bundle install`

## Gems

- autotest-fsevent
- autotest-growl
- autotest-rails
- awesome_print
- bundler
- cheat
- guard
- nyan-cat-formatter
- pry puma
- rb-appscript
- rb-fsevent
- rb-inotify
- rb-kqueue
- rb-readline
- terminal-notifier-guard
- tmuxinator
- working_man
