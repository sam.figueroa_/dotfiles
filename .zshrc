## Path to your oh-my-zsh configuration.
export ZSH=$HOME/.oh-my-zsh

# Powerlevel10K ZSH Theme 
# https://github.com/romkatv/powerlevel10k
typeset -g POWERLEVEL9K_INSTANT_PROMPT=off
export POWERLEVEL9K_INSTANT_PROMPT=off
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  # source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi

# Set to the name theme to load.
# Look in ~/.oh-my-zsh/themes/
# export ZSH_THEME="sporty_sam"
export ZSH_THEME="powerlevel10k"

# Set to this to use case-sensitive completion
# export CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
export DISABLE_AUTO_UPDATE="true"

# enable speaking deploy messages
export TALKTOMEBABY=1

# report time if execution exceeds amount of seconds
export REPORTTIME=2

export LANG="en_US.UTF-8"
export EDITOR='nvim'
# export LC_ALL='en_US'
export LC_ALL="en_US.UTF-8"
export DISABLE_AUTO_TITLE=true
export CC=/usr/bin/gcc
export COMPLETION_WAITING_DOTS="true"
export SPRING_ON=1
export SPRING=1
export BAT_THEME="Monokai Extended"
export TERM="screen-256color"
export BETTER_ERRORS_EDITOR='vim'

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git rails ruby brew zsh-syntax-highlighting fasd)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=$HOME/script:$HOME/bin:/usr/local/bin:/usr/local/sbin:/usr/local/lib:/Users/neo/script:/Users/neo/Dropbox/script:/Users/neo/script/perl:/usr/bin:/usr/sbin:/bin:/sbin:/usr/X11/bin:/usr/local/Cellar/node/0.8.8/bin:/usr/local/share/npm/bin:$PATH
export PATH="/opt/homebrew/bin:$PATH"
export PATH="/Applications/SnowSQL.app/Contents/MacOS/:$PATH"

# one'zies
alias e='subl .'
alias o='open .' #open current directory in finder
alias vi=$EDITOR
alias ctags="`brew --prefix`/bin/ctags"

# rails development aliases
alias rr='rake routes'
alias lh3='open http://localhost:3000'
alias preflight_check='bundle install; rake db:migrate'

#git stuff
alias eat='clear; git pull ; glg --no-merges --max-count=7'
alias poop='git push'
alias cut='git branch -d'
alias branch='git co -b'
alias hard_reset='git reset --hard && git clean -d -f'
alias gti='git'
alias gdf='git diff'
alias gst='rubocop-git ; git status'
alias sb='git branch --all | fzf | xargs git checkout $1' 
alias glu='git pull && bundle install && yarn install && rails db:migrate && git co db/structure.sql && g co db/structure.sql' 
alias recent="git for-each-ref --sort=-committerdate refs/heads/ --format='%(HEAD) %(refname:short) %(objectname:short) - %(contents:subject) - %(authorname) (%(committerdate:relative))' | head -n 10 | fzf --reverse --height 30% | awk '{print \$1;}' | xargs git checkout"
alias gdc='git diff --cached'

# sound it out
alias bell='mpg123 ~/.files/sounds/command_complete.mp3'
alias beep='bell'
alias notify='say "command completed"'

# misc
alias cal='ncal'
alias caly='ncal -y'

# 4 T3H LOLZ
alias cat='bat'
alias pry_shell='pry-shell'

# pomodoro
alias work="timer 30m && terminal-notifier -message 'Pomodoro'\
        -title 'Work Timer is up! Take a Break 😊'\
        -appIcon '~/Pictures/timer.png'\
        -sound Crystal"

# GitLab
alias self='cd /Users/neo/gdk_self_managed/gitlab' 
alias snowsql='/Applications/SnowSQL.app/Contents/MacOS/snowsql'

fpath=(/usr/local/share/zsh-completions $fpath)

[[ -s $HOME/.tmuxinator/scripts/tmuxinator ]] && source $HOME/.tmuxinator/scripts/tmuxinator
function _update_ruby_version()
{
    typeset -g ruby_version=''
    if which asdf &> /dev/null; then
      ruby_version="$(asdf which ruby | grep -o -e "[0-9]\.[0-9]\.[0-9]")"
    fi
}
chpwd_functions+=(_update_ruby_version)

function howoften()
{
  history | awk '{a[$2]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head
}

function showlogo() {
  if [[ -n $SKIP_LOGO && $SKIP_LOGO == "true" ]]; then
    return 0
  fi

  if [[ -d ~/.files/logos ]]; then
    LOGOS=( ~/.files/logos/* )
    /bin/cat $LOGOS[$RANDOM%$#LOGOS+1]
  else
    figlet -f larry3d `hostname -s`
  fi

  echo ""
}

function lintchanges() {
  git diff --name-only | xargs yarn run lint:eslint
}

showlogo
caly

if which eza &> /dev/null; then
  # brew install font-jetbrains-mono-nerd-font for icons
  alias l="eza -l --git --header --all --all --time-style '+%Y-%m-%d %H:%M' --no-user --no-permissions --icons"
  alias ll="eza -l --git --header --all --all --time-style '+%Y-%m-%d %H:%M' -o --group"
fi


export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/opt/homebrew/share/zsh-syntax-highlighting/highlighters
source /opt/homebrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
. /opt/homebrew/opt/asdf/libexec/asdf.sh

### MANAGED BY RANCHER DESKTOP START (DO NOT EDIT)
export PATH="/Users/neo/.rd/bin:$PATH"
### MANAGED BY RANCHER DESKTOP END (DO NOT EDIT)

export JAVA_HOME="/Users/neo/.asdf/installs/java/adoptopenjdk-11.0.22+7"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export CONFIGURE_OPTS="--with-openssl=$(brew --prefix openssl)"
export PATH="$HOME/.pyenv/bin:$PATH"
# export PATH="/Users/neo/.pyenv/versions/3.10.4/bin:$PATH"

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
eval $(thefuck --alias)

[ -f ~/focus/priority.md ] && /bin/cat ~/focus/priority.md
