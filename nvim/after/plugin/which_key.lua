-- require('which-key').setup {
disbled = {
  window = {
    border = "double",
    position = "bottom",
  },
  layout = {
    align = "left",
    height = { min = 10, max = 20 },
    width = { min = 25, max = 60 },
    spacing = 5
  },
  plugins = {
    marks = true,
    spelling = {
      enabled = true,
      suggestions = 30
    },
    presets = {
      windows = true,
      nav = true,
      z = true
    }
  }
}
