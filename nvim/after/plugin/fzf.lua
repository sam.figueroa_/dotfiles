local fzf = require('fzf-lua')

-- The reason I added  'opts' as a parameter is so you can
-- call this function with your own parameters / customizations
-- for example: 'git_files_cwd_aware({ cwd = <another git repo> })'
function fzf.git_files_cwd_aware(opts)
  opts = opts or {}
  local fzf_lua = require('fzf-lua')
  -- git_root() will warn us if we're not inside a git repo
  -- so we don't have to add another warning here, if
  -- you want to avoid the error message change it to:
  -- local git_root = fzf_lua.path.git_root(opts, true)
  local git_root = fzf_lua.path.git_root(opts)
  if not git_root then return end
  local relative = fzf_lua.path.relative(vim.loop.cwd(), git_root)
  opts.fzf_opts = { ['--query'] = git_root ~= relative and relative or nil }
  return fzf_lua.git_files(opts)
end

vim.keymap.set('n', '<leader>v', function() fzf.registers() end, { noremap = true, silent = true })
vim.keymap.set('n', '<leader>O', function() fzf.files() end, { noremap = true, silent = true })
-- vim.keymap.set('n', '<leader>o', function() fzf.git_files_cwd_aware() end, { noremap = true, silent = true })
vim.keymap.set('n', '<leader>o', function() fzf.git_files() end, { noremap = true, silent = true })
vim.keymap.set('n', '<leader>b', function() fzf.blines() end, { noremap = true, silent = true })
vim.keymap.set('n', '<leader>B', function() fzf.buffers() end, { noremap = true, silent = true })
vim.keymap.set('n', '<leader>f', function() fzf.live_grep() end, { noremap = true, silent = true })
vim.keymap.set({'n','v'}, '<leader>F', function() fzf.grep_cword() end, { noremap = true, silent = true })

-- vim.api.nvim_set_keymap('n', '<leader>O', "<cmd>lua require('fzf-lua').files()<CR>", { noremap = true, silent = true })
-- vim.api.nvim_set_keymap('n', '<leader>o', "<cmd>lua require('fzf-lua').git_files()<CR>", { noremap = true, silent = true })
--
vim.env.FZF_DEFAULT_OPTS = "--ansi --preview-window 'right:40%' --layout reverse --margin=1,4 --preview 'bat --color=always --style=grid --line-range :10 {}'"
-- vim.g.fzf_layout = { 'down': '~40%' }

