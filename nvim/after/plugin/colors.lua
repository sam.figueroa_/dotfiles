function ColorMyPencils(color)
  -- color = color or "rose-pine"
  -- color = color or "tokyonight-night"
  -- color = color or "sobrio"
  -- color = color or "vim-monokai-tasty"
  -- color = color or "kanagawa-dragon"
  -- vim.cmd.colorscheme("kanagawa")
  color = color or "dracula"
  vim.cmd.colorscheme(color)
  -- vim.cmd.AirlineTheme('tomorrow')
  -- vim.cmd.AirlineTheme('sobrio')
  vim.cmd.AirlineTheme('dracula')

  vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
  vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end

ColorMyPencils()

