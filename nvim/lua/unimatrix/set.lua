vim.opt.nu = true
vim.opt.smarttab = true
vim.opt.shiftwidth = 2
vim.opt.list = true
vim.opt.listchars = "tab:>~"
vim.opt.scrolloff = 10
vim.opt.splitright = true
vim.opt.splitbelow = true

vim.opt.wrap = true
vim.opt.background = "dark"
vim.opt.cursorline = true

vim.opt.ruler = true

vim.opt.colorcolumn = '120'
vim.g.highlight = "ColorColumn ctermbg=0 guibg=lightgrey"
vim.g.extra_whitespace_ignored_filetypes = {'fugitive', 'diff', 'qf', 'help', 'ctrlsf'}

vim.opt.timeout = true
vim.opt.timeoutlen = 300
vim.opt.foldcolumn = '1'
