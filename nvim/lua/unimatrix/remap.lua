vim.g.mapleader = ' '

-- Create a function to set keymaps with sane defaults
local map = function(mode, lhs, rhs, desc, opts)
  local local_opts = {}
  if desc then
      local_opts.desc = desc
  end
  if opts then
    for k, v in pairs(opts) do
        local_opts[k] = v
    end
  end
  vim.keymap.set(mode, lhs, rhs, local_opts)
end

local nmap = function(lhs, rhs, desc, opts)
    map("n", lhs, rhs, desc, opts)
end

local imap = function(lhs, rhs, desc, opts)
    map("i", lhs, rhs, desc, opts)
end

nmap('<leader>pv', vim.cmd.Ex)

-- Do not use arrows in Normal mode
-- vim.keymap.set('n' ,'<silent> <Up>    <Nop>')
-- vim.keymap.set('n' ,'<silent> <Down>  <Nop>')
-- vim.keymap.set('n' ,'<silent> <Left>  <Nop>')
-- vim.keymap.set('n' ,'<silent> <Right> <Nop>')

-- Smart way to move between panes
map({'i','v','n'}, '<up>', '<C-w><up>', 'Jump up to next pane')
map({'i','v','n'}, '<down>', '<C-w><down>', 'Jump down to next pane')
map({'i','v','n'}, '<left>', '<C-w><left>', 'Jump left to next pane')
map({'i','v','n'}, '<right>', '<C-w><right>', 'Jump right to next pane')

-- quick tab switching
map({'v','n'}, '<c-right>', ':tabnext<cr>', 'Jump to next tab')
map({'v','n'}, '<c-left>', ':tabprevious<cr>', 'Jump to previous tab')
map({'v','n'}, '<leader>0', ':tablast<cr>', 'Jump to last tab')
map({'v','n'}, '<leader>1', '1gt', 'Jump to 1st tab')
map({'v','n'}, '<leader>2', '2gt', 'Jump to 2nd tab')
map({'v','n'}, '<leader>3', '3gt', 'Jump to 3rd tab')
map({'v','n'}, '<leader>4', '4gt', 'Jump to 4th tab')
map({'v','n'}, '<leader>5', '5gt', 'Jump to 5th tab')
map({'v','n'}, '<leader>6', '6gt', 'Jump to 6th tab')
map({'v','n'}, '<leader>7', '7gt', 'Jump to 7th tab')
map({'v','n'}, '<leader>8', '8gt', 'Jump to 8th tab')
map({'v','n'}, '<leader>9', '9gt', 'Jump to 9th tab')
map({'v','n'}, '<leader>t', ':tablast<cr>:tabe<cr>', 'Append new tab to list')


-- switch panes without arrow keys
map({'i','v','n'}, '<c-k>', '<C-w><up>', 'Jump up to next pane')
map({'i','v','n'}, '<c-j>', '<C-w><down>', 'Jump down to next pane')
map({'i','v','n'}, '<c-h>', '<C-w><left>', 'Jump left to next pane')
map({'i','v','n'}, '<c-l>', '<C-w><right>', 'Jump right to next pane')

-- grow panes quickly
map({'i','v','n'}, '<leader>K', '50<C-w>-', 'Grow up')
map({'i','v','n'}, '<leader>J', '50<C-w>+', 'Grow down')
map({'i','v','n'}, '<leader>H', '50<C-w><', 'Grow left')
map({'i','v','n'}, '<leader>L', '50<C-w>>', 'Grow right')

-- tiny plug in mappings that don't warrent their own configs(yet)
nmap('<leader>c', '<cmd>Commentary<cr>', 'Comment toggle')
nmap('<leader>w', '<cmd>StripWhitespace<cr>', 'Remove trailing whitespace chars')

-- linters
nmap('<leader>R', '<cmd>!bundle exec rubocop %<cr>', 'Check file with rubocop')
nmap('<leader>E', '<cmd>!yarn run lint:eslint %<cr>', 'Check file with eslint')
nmap('<leader>U', '<cmd>!yarn prettier --write %<cr>', 'Prettyfy file')

-- specs
nmap('<leader>r', '<cmd>!bundle exec rspec %<cr>', 'Run file in RSpec')

-- folding
nmap('<leader>~', ':set foldmethod=indent<cr>', 'Enable folds')
nmap('~', 'za', 'Toggle fold')

-- show linter/parser errors
nmap('<leader>se', '<cmd>lua vim.diagnostic.open_float()<CR>', 'Show errors')
