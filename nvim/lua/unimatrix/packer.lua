-- This file can be loaded by calling `lua require('plugins')` from your init.vim
--) Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- file navigation
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.0',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  use 'theprimeagen/harpoon'

  -- colors
  use 'crusoexia/vim-monokai'
  use({ 'rose-pine/neovim', as = 'rose-pine' })
  use 'folke/tokyonight.nvim'
  use 'elvessousa/sobrio'
  use 'patstockwell/vim-monokai-tasty'
  use 'folke/lsp-colors.nvim'
  use 'Rigellute/shades-of-purple.vim'
  use 'rebelot/kanagawa.nvim'
  use({'dracula/vim', as = 'dracula' })

  -- syntax highlighting and code readability improvements
  -- use 'wellle/context.vim'
  use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
  use { 'ibhagwan/fzf-lua',
  -- optional for icon support
    requires = { 'nvim-tree/nvim-web-devicons' }
  }
  use { 'junegunn/fzf', run = './install --bin' }

  use 'nvim-lua/plenary.nvim'
  use 'tpope/vim-fugitive'
  use 'tpope/vim-commentary'
  use 'tpope/vim-projectionist'
  use 'xolox/vim-misc'
  use 'xolox/vim-session'
  use 'airblade/vim-gitgutter'
  use 'ntpeters/vim-better-whitespace'
  use 'vim-test/vim-test'

  use 'vim-airline/vim-airline'
  use 'vim-airline/vim-airline-themes'
  use 'ruanyl/vim-gh-line'

  -- use { "git@gitlab.com:gitlab-org/editor-extensions/gitlab.vim.git" }

  -- use "folke/which-key.nvim"

  -- LSP setup
  use {
  'VonHeikemen/lsp-zero.nvim',
  requires = {
    -- LSP Support
    {'neovim/nvim-lspconfig'},
    {'williamboman/mason.nvim'},
    {'williamboman/mason-lspconfig.nvim'},

    -- Autocompletion
    {'hrsh7th/nvim-cmp'},
    {'hrsh7th/cmp-path'},
    {'hrsh7th/cmp-buffer'},
    {'hrsh7th/cmp-nvim-lsp'},
    {'hrsh7th/cmp-nvim-lua'},
    {'saadparwaiz1/cmp_luasnip'},

    -- Snippets
    {'L3MON4D3/LuaSnip'},
    {'rafamadriz/friendly-snippets'},
  }
}
end)

